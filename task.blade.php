@extends('admin/master')
@section('task')
 <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <!-- <h4 class="page-title">Form Wizard</h4> -->
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="card">
                    <div class="card-body wizard-content">
                        <h4 class="card-title">ADD TASK</h4>
                        <h6 class="card-subtitle"></h6>
                        <form id="example-form" action="{{ route ('Admin')}}" class="m-t-40" method="post">
                        {{session()->get('message')}}
                        @csrf
                            <div>
                                <!-- <h3>Account</h3> -->
                                <section>
                                    <label for="userName">Task name</label>
                                    <input id="userName" name="taskname" type="text" class= "form-control">
                                    <label for="description">description</label>
                                    <input id="description" name="description" type="text" class=" form-control">
                                    <label for="duration">Duration</label>
                                    <input id="duration" name="duration" type="text" class=" form-control">
                                    <label for="createby">createby</label>
                                    <input id="createby" name="createby" type="text" class=" form-control">
                                    <label for="asssignto">Assign to</label>
                                    <input id="assignto" name="assignto" type="text" class="form-control"><br>
                                    <div class="form-button">
                                    <input id="confirm" name="submit" type="submit" class="btn btn-primary align:center">
</div>


                            </div>
                        </form>
                    </div>
                </div>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css"/>

    <div class="main-container" style="border:1px black;">
        <main class="pd-ltr-20 xs-pd-20-10">
            <div class="min-height-200px">
                <div class="page-header">
                    <div class="row">
                    <div class="col-md-12 col-sm-12" style="margin-left">

                            <br><br>
                             <div class="pd-20 card-box mb-30">
                                <div class="clearfix mb-20">
                                    <div class="pull-left">
                                        <h4 class="text-blue h4">Task</h4>
                                    </div>

                                    <a href="{{url('admin')}}/task">
                                     <i class="btn btn-outline-primary" style="margin-left: 20px"> + Add Task</i>
                                    </a>
                                    <span style="color:#1b00ff;margin-left: 60px;text-decoration:underline"> {{session()->get('message')}}</span>
                                    <br><br>
                                    <table class="table responsive" id="utable">
                                        <thead>
                                        <tr>
                                            <th class="all">ID</th>
                                            <th class="all">Task Name</th>
                                            <th class="none">Description</th>
                                            <th class="all">Duration</th>
                                            <th class="all">createby</th>
                                            <th class="all">assignto</th>
                                             <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php  $i=1; @endphp
                                        @foreach($task as $val)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <!-- <td>{{$val->id}}</td> -->
                                                <!-- <td>{{$val->id}}</td> -->
                                                <td>{{$val->taskname }}</td>
                                                <td>{{$val->description}}</td>
                                                <td>{{$val->duration }}</td>
                                                <td>{{$val->createby }}</td>
                                                <td>{{$val->assignto }}</td>

                                                <td>

                                                    <a  href="{{url('Admin/'.$val->id.'/edit')}}"><i class=" f-icon fas fa-edit"></i> </a>
                                                    @method('DELETE')
                                                    <a  href="{{url('Admin/'.$val->id)}}"><i class="f-icon fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script>
        $(document).ready(function(){
            $('#utable').DataTable();
        });
</script>
@endsection
