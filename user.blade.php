@extends('admin/master')
@section('user')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css"/>

    <div class="main-container" style="border:1px black;">
        <main class="pd-ltr-20 xs-pd-20-10">
            <div class="min-height-200px">
                <div class="page-header">
                    <div class="row">
                        <div class="col-md-6 col-sm-6" style="margin-left:500px;">
                            <!-- <a href="{}}/newuser">
                                <i class="btn btn-outline-secondary" style="margin-left: 20px"> + Add Users</i>
                            </a>
                            <br><br> -->
                             <div class="pd-20 card-box mb-30">
                                <div class="clearfix mb-20">
                                    <div class="pull-left">
                                        <h4 class="text-blue h4">Users</h4>
                                    </div>
                                    <span style="color:#1b00ff;margin-left: 60px;text-decoration:underline"> {{session()->get('message')}}</span>
                                    <br><br>
                                    <table class="table responsive" id="utable">
                                        <thead>
                                        <tr>
                                            <th class="all">ID</th>
                                            <th class="all">Name</th>
                                            <th class="all">Email</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php  $i=1; @endphp
                                        @foreach($user as $val)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <!-- <td>{{$val->id}}</td> -->
                                                <td>{{$val->name}}</td>
                                                <td>{{$val->email }}</td>

                                                <!-- <td>
                                                    <a  href="#"><i class="dw dw-edit2"></i> </a>&nbsp;
                                                    {{--<a  href="{{url('/Admin/deleteproduct')}}/{{$val->p_id}}"><i class="dw dw-delete-3"></i></a>--}}
                                                </td> -->
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script>
        $(document).ready(function(){
            $('#utable').DataTable();
        });
    </script>
@endsection
