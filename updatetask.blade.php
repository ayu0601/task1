@extends('admin/master')
@section('task')
 <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <!-- <h4 class="page-title">Form Wizard</h4> -->
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="card">
                    <div class="card-body wizard-content">
                        <h4 class="card-title">UPDATE TASK</h4>
                        <h6 class="card-subtitle"></h6>
                        <form id="example-form" action="{{url('Admin/'.$task->id)}}" class="m-t-40" method="post">
                        {{session()->get('message')}}
                        @csrf
                        @method('PUT')
                            <div>
                                <!-- <h3>Account</h3> -->
                                <section>
                                    <label for="userName">Task name</label>
                                    <input id="userName" name="taskname" type="text" class= "form-control"  value="{{$task->taskname}}">
                                    <label for="password">description</label>
                                    <input id="password" name="description" type="text" class=" form-control" value="{{$task->description}}">
                                    <label for="confirm">Duration</label>
                                    <input id="confirm" name="duration" type="text" class="required form-control" value="{{$task->duration}}">
                                    <label for="confirm">createby</label>
                                    <input id="confirm" name="createby" type="text" class="required form-control" value="{{$task->createby}}">
                                    <label for="confirm">Assign to</label>
                                    <input id="confirm" name="assignto" type="text" class="required form-control" value="{{$task->assignto}}"><br>
                                    <div class="form-button">
                                    <input id="confirm" name="submit" value="savechanges" type="submit" class="btn btn-primary align:center">
</div>


                            </div>
                        </form>
                    </div>
                </div>
                @endsection

